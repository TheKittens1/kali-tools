---
Title: radare2
Homepage: https://www.radare.org
Repository: https://salsa.debian.org/pkg-security-team/radare2
Architectures: any all
Version: 5.9.0+dfsg-2
Metapackages: kali-linux-default kali-linux-everything kali-linux-headless kali-linux-large kali-tools-forensics kali-tools-hardware kali-tools-respond kali-tools-reverse-engineering 
Icon: images/radare2-logo.svg
PackagesInfo: |
 ### libradare2-5.0.0t64
 
  The project aims to create a complete, portable, multi-architecture,
  unix-like toolchain for reverse engineering.
   
  It is composed by an hexadecimal editor (radare) with a wrapped IO
  layer supporting multiple backends for local/remote files, debugger
  (OS X, BSD, Linux, W32), stream analyzer, assembler/disassembler (rasm)
  for x86, ARM, PPC, m68k, Java, MSIL, SPARC, code analysis modules and
  scripting facilities. A bindiffer named radiff, base converter (rax),
  shellcode development helper (rasc), a binary information extractor
  supporting PE, mach0, ELF, class, etc. named rabin, and a block-based
  hash utility called rahash.
   
  This package provides the libraries from radare2.
 
 **Installed size:** `19.77 MB`  
 **How to install:** `sudo apt install libradare2-5.0.0t64`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libcapstone4 
 * liblz4-1 
 * libmagic1t64 
 * libradare2-common 
 * libxxhash0 
 * libzip4t64 
 * zlib1g 
 {{< /spoiler >}}
 
 
 - - -
 
 ### libradare2-common
 
  The project aims to create a complete, portable, multi-architecture,
  unix-like toolchain for reverse engineering.
   
  It is composed by an hexadecimal editor (radare) with a wrapped IO
  layer supporting multiple backends for local/remote files, debugger
  (OS X, BSD, Linux, W32), stream analyzer, assembler/disassembler (rasm)
  for x86, ARM, PPC, m68k, Java, MSIL, SPARC, code analysis modules and
  scripting facilities. A bindiffer named radiff, base converter (rax),
  shellcode development helper (rasc), a binary information extractor
  supporting PE, mach0, ELF, class, etc. named rabin, and a block-based
  hash utility called rahash.
   
  This package provides the arch independent files from radare2.
 
 **Installed size:** `8.35 MB`  
 **How to install:** `sudo apt install libradare2-common`  
 
 
 - - -
 
 ### libradare2-dev
 
  The project aims to create a complete, portable, multi-architecture,
  unix-like toolchain for reverse engineering.
   
  It is composed by an hexadecimal editor (radare) with a wrapped IO
  layer supporting multiple backends for local/remote files, debugger
  (OS X, BSD, Linux, W32), stream analyzer, assembler/disassembler (rasm)
  for x86, ARM, PPC, m68k, Java, MSIL, SPARC, code analysis modules and
  scripting facilities. A bindiffer named radiff, base converter (rax),
  shellcode development helper (rasc), a binary information extractor
  supporting PE, mach0, ELF, class, etc. named rabin, and a block-based
  hash utility called rahash.
   
  This package provides the devel files from radare2.
 
 **Installed size:** `1.38 MB`  
 **How to install:** `sudo apt install libradare2-dev`  
 
 {{< spoiler "Dependencies:" >}}
 * libcapstone-dev
 * liblz4-dev
 * libmagic-dev
 * libradare2-5.0.0t64 
 * libuv1-dev
 * libzip-dev
 {{< /spoiler >}}
 
 
 - - -
 
 ### radare2
 
  The project aims to create a complete, portable, multi-architecture,
  unix-like toolchain for reverse engineering.
   
  It is composed by an hexadecimal editor (radare) with a wrapped IO
  layer supporting multiple backends for local/remote files, debugger
  (OS X, BSD, Linux, W32), stream analyzer, assembler/disassembler (rasm)
  for x86, ARM, PPC, m68k, Java, MSIL, SPARC, code analysis modules and
  scripting facilities. A bindiffer named radiff, base converter (rax),
  shellcode development helper (rasc), a binary information extractor
  supporting PE, mach0, ELF, class, etc. named rabin, and a block-based
  hash utility called rahash.
 
 **Installed size:** `3.14 MB`  
 **How to install:** `sudo apt install radare2`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 * libradare2-5.0.0t64 
 {{< /spoiler >}}
 
 ##### r2
 
 Advanced command-line hexadecimal editor, disassembler and debugger
 
 ```
 root@kali:~# r2 -h
 Usage: r2 [-ACdfjLMnNqStuvwzX] [-P patch] [-p prj] [-a arch] [-b bits] [-c cmd]
           [-s addr] [-B baddr] [-m maddr] [-i script] [-e k=v] file|pid|-|--|=
  --           run radare2 without opening any file
  -            same as 'r2 malloc://512'
  =            read file from stdin (use -i and -c to run cmds)
  -=           perform !=! command to run all commands remotely
  -0           print \x00 after init and every command
  -2           close stderr file descriptor (silent warning messages)
  -a [arch]    set asm.arch
  -A           run 'aaa' command to analyze all referenced code
  -b [bits]    set asm.bits
  -B [baddr]   set base address for PIE binaries
  -c 'cmd..'   execute radare command
  -C           file is host:port (alias for -c+=http://%s/cmd/)
  -d           debug the executable 'file' or running process 'pid'
  -D [backend] enable debug mode (e cfg.debug=true)
  -e k=v       evaluate config var
  -f           block size = file size
  -F [binplug] force to use that rbin plugin
  -h, -hh      show help message, -hh for long
  -H ([var])   display variable
  -i [file]    run script file
  -I [file]    run script file before the file is opened
  -j           use json for -v, -L and maybe others
  -k [OS/kern] set asm.os (linux, macos, w32, netbsd, ...)
  -l [lib]     load plugin file
  -L, -LL      list supported IO plugins (-LL list core plugins)
  -m [addr]    map file at given address (loadaddr)
  -M           do not demangle symbol names
  -n, -nn      do not load RBin info (-nn only load bin structures)
  -N           do not load user settings and scripts
  -NN          do not load any script or plugin
  -q           quiet mode (no prompt) and quit after -i
  -qq          quit after running all -c and -i
  -Q           quiet mode (no prompt) and quit faster (quickLeak=true)
  -p [prj]     use project, list if no arg, load if no file
  -P [file]    apply rapatch file and quit
  -r [rarun2]  specify rarun2 profile to load (same as -e dbg.profile=X)
  -R [rr2rule] specify custom rarun2 directive
  -s [addr]    initial seek
  -S           start r2 in sandbox mode
  -t           load rabin2 info in thread
  -u           set bin.filter=false to get raw sym/sec/cls names
  -v, -V       show radare2 version (-V show lib versions)
  -w           open file in write mode
  -x           open without exec-flag (asm.emu will not work), See io.exec
  -X           same as -e bin.usextr=false (useful for dyldcache)
  -z, -zz      do not load strings or load them even in raw
 ```
 
 - - -
 
 ##### r2agent
 
 Radare2 remoting manager TODO
 
 ```
 root@kali:~# r2agent -h
 Usage: r2agent [-adhs] [-p port]
   -a        listen for everyone (localhost by default)
   -d        run in daemon mode (background)
   -h        show this help message
   -s        run in sandbox mode
   -u        enable http authorization access
   -t        user:password authentication file
   -p [port] specify listening port (defaults to 8080)
 ```
 
 - - -
 
 ##### r2pm
 
 Radare2 package manager
 
 ```
 root@kali:~# r2pm -h
 Usage: r2pm [-flags] [pkgs...]
 Commands:
  -a [repository]   add or -delete external repository
  -c ([git/dir])    clear source cache (R2PM_GITDIR)
  -ci <pkgname>     clean + install
  -cp               clean the user's home plugin directory
  -d,doc [pkgname]  show documentation and source for given package
  -e [pkgname]      edit using $EDITOR the given package script
  -f                force operation (Use in combination of -U, -i, -u, ..)
  -gi <pkg>         global install (system-wide)
  -h                display this help message
  -H variable       show the value of given internal environment variable (See -HH)
  -HH               show all the internal environment variable values
  -i <pkgname>      install/update package and its dependencies (see -c, -g)
  -I                information about repository and installed packages
  -l                list installed packages
  -q                be quiet
  -r [cmd ...args]  run shell command with R2PM_BINDIR in PATH
  -s [<keyword>]    search available packages in database matching a string
  -t [YYYY-MM-DD]   force a moment in time to pull the code from the git packages
  -u <pkgname>      r2pm -u baleful (See -f to force uninstall)
  -uci <pkgname>    uninstall + clean + install
  -ui <pkgname>     uninstall + install
  -U                initialize/update database and upgrade all outdated packages
  -v                show version
 ```
 
 - - -
 
 ##### r2r
 
 
 ```
 root@kali:~# r2r -h
 Usage: r2r [-qvVnLi] [-C dir] [-F dir] [-f file] [-o file] [-s test] [-t seconds] [-j threads] [test file/dir | @test-type]
  -C [dir]     chdir before running r2r (default follows executable symlink + test/new
  -F [dir]     run fuzz tests (open and default analysis) on all files in the given dir
  -L           log mode (better printing for CI, logfiles, etc.)
  -V           verbose
  -f [file]    file to use for json tests (default is bins/elf/crackme0x00b)
  -g           run the tests specified via '// R2R' comments in modified source files
  -h           print this help
  -i           interactive mode
  -j [threads] how many threads to use for running tests concurrently (default is 8)
  -n           do nothing (don't run any test, just load/parse them)
  -o [file]    output test run information in JSON format to file
  -q           quiet
  -s [test]    set R2R_SKIP_(TEST)=1 to skip running that test type
  -t [seconds] timeout per test (default is (60*60))
  -u           do not git pull/clone test/bins (See R2R_OFFLINE)
  -v           show version
 
 R2R_SKIP_ARCHOS=1  # do not run the arch-os-specific tests
 R2R_SKIP_JSON=1    # do not run the JSON tests
 R2R_SKIP_FUZZ=1    # do not run the fuzz tests
 R2R_SKIP_UNIT=1    # do not run the unit tests
 R2R_SKIP_CMD=1     # do not run the cmds tests
 R2R_SKIP_ASM=1     # do not run the rasm2 tests
 R2R_TIMEOUT=3600   # timeout after 1 minute (60 * 60)
 R2R_OFFLINE=1      # same as passing -u
 
 Supported test types: @asm @json @unit @fuzz @arch @cmd
 OS/Arch for archos tests: linux-x86_64
 ```
 
 - - -
 
 ##### rabin2
 
 Binary program info extractor
 
 ```
 root@kali:~# rabin2 -h
 Usage: rabin2 [-AcdeEghHiIjlLMqrRsSUvVxzZ] [-@ at] [-a arch] [-b bits] [-B addr]
               [-C F:C:D] [-f str] [-m addr] [-n str] [-N m:M] [-P[-P] pdb]
               [-o str] [-O str] [-k query] [-D lang mangledsymbol] file
  -@ [addr]       show section, symbol or import at addr
  -A              list sub-binaries and their arch-bits pairs
  -a [arch]       set arch (x86, arm, .. or <arch>_<bits>)
  -b [bits]       set bits (32, 64 ...)
  -B [addr]       override base address (pie bins)
  -c              list classes
  -cc             list classes in header format
  -C [fmt:C:D]    create [elf,mach0,pe] with Code and Data hexpairs (see -a)
  -d              show debug/dwarf information
  -D lang name    demangle symbol name (-D all for bin.demangle=true)
  -e              program entrypoint
  -ee             constructor/destructor entrypoints
  -E              globally exportable symbols
  -f [str]        select sub-bin named str
  -F [binfmt]     force to use that bin plugin (ignore header check)
  -g              same as -SMZIHVResizcld -SS -SSS -ee (show all info)
  -G [addr]       load address . offset to header
  -h              this help message
  -H              header fields
  -i              imports (symbols imported from libraries)
  -I              binary info
  -j              output in json
  -k [sdb-query]  run sdb query. for example: '*'
  -K [algo]       calculate checksums (md5, sha1, ..)
  -l              linked libraries
  -L [plugin]     list supported bin plugins or plugin details
  -m [addr]       show source line at addr
  -M              main (show address of main symbol)
  -n [str]        show section, symbol or import named str
  -N [min:max]    force min:max number of chars per string (see -z and -zz)
  -o [str]        output file/folder for write operations (out by default)
  -O [str]        write/extract operations (-O help)
  -p              show always physical addresses
  -P              show debug/pdb information
  -PP             download pdb file for binary
  -q              be quiet, just show fewer data
  -qq             show less info (no offset/size for -z for ex.)
  -Q              show load address used by dlopen (non-aslr libs)
  -r              radare output
  -R              relocations
  -s              symbols
  -S              sections
  -SS             segments
  -SSS            sections mapping to segments
  -t              display file hashes
  -T              display file signature
  -u              unfiltered (no rename duplicated symbols/sections)
  -U              resoUrces
  -v              display version and quit
  -V              show binary version information
  -w              display try/catch blocks
  -x              extract bins contained in file
  -X [fmt] [f] .. package in fat or zip the given files and bins contained in file
  -z              strings (from data section)
  -zz             strings (from raw bins [e bin.str.raw=1])
  -zzz            dump raw strings to stdout (for huge files)
  -Z              guess size of binary program
 Environment:
  R2_NOPLUGINS:     1|0|               # do not load shared plugins (speedup loading)
  RABIN2_ARGS:                         # ignore cli and use these program arguments
  RABIN2_CHARSET:   e cfg.charset      # set default value charset for -z strings
  RABIN2_DEBASE64:  e bin.str.debase64 # try to debase64 all strings
  RABIN2_DEMANGLE=0:e bin.demangle     # do not demangle symbols
  RABIN2_DMNGLRCMD: e bin.demanglercmd # try to purge false positives
  RABIN2_LANG:      e bin.lang         # assume lang for demangling
  RABIN2_MAXSTRBUF: e bin.str.maxbuf   # specify maximum buffer size
  RABIN2_PDBSERVER: e pdb.server       # use alternative PDB server
  RABIN2_PREFIX:    e bin.prefix       # prefix symbols/sections/relocs with a specific string
  RABIN2_STRFILTER: e bin.str.filter   # r2 -qc 'e bin.str.filter=??' -
  RABIN2_MACHO_NOFUNCSTART:           # if set it will ignore the FUNCSTART information
  RABIN2_MACHO_NOSWIFT
  RABIN2_MACHO_SKIPFIXUPS
  RABIN2_CODESIGN_VERBOSE
  RABIN2_STRPURGE:  e bin.str.purge    # try to purge false positives
  RABIN2_SYMSTORE:  e pdb.symstore     # path to downstream symbol store
  RABIN2_SWIFTLIB:  1|0|               # load Swift libs to demangle (default: true)
  RABIN2_VERBOSE:   e bin.verbose      # show debugging messages from the parser
 ```
 
 - - -
 
 ##### radare2
 
 Advanced command-line hexadecimal editor, disassembler and debugger
 
 ```
 root@kali:~# radare2 -h
 Usage: r2 [-ACdfjLMnNqStuvwzX] [-P patch] [-p prj] [-a arch] [-b bits] [-c cmd]
           [-s addr] [-B baddr] [-m maddr] [-i script] [-e k=v] file|pid|-|--|=
  --           run radare2 without opening any file
  -            same as 'r2 malloc://512'
  =            read file from stdin (use -i and -c to run cmds)
  -=           perform !=! command to run all commands remotely
  -0           print \x00 after init and every command
  -2           close stderr file descriptor (silent warning messages)
  -a [arch]    set asm.arch
  -A           run 'aaa' command to analyze all referenced code
  -b [bits]    set asm.bits
  -B [baddr]   set base address for PIE binaries
  -c 'cmd..'   execute radare command
  -C           file is host:port (alias for -c+=http://%s/cmd/)
  -d           debug the executable 'file' or running process 'pid'
  -D [backend] enable debug mode (e cfg.debug=true)
  -e k=v       evaluate config var
  -f           block size = file size
  -F [binplug] force to use that rbin plugin
  -h, -hh      show help message, -hh for long
  -H ([var])   display variable
  -i [file]    run script file
  -I [file]    run script file before the file is opened
  -j           use json for -v, -L and maybe others
  -k [OS/kern] set asm.os (linux, macos, w32, netbsd, ...)
  -l [lib]     load plugin file
  -L, -LL      list supported IO plugins (-LL list core plugins)
  -m [addr]    map file at given address (loadaddr)
  -M           do not demangle symbol names
  -n, -nn      do not load RBin info (-nn only load bin structures)
  -N           do not load user settings and scripts
  -NN          do not load any script or plugin
  -q           quiet mode (no prompt) and quit after -i
  -qq          quit after running all -c and -i
  -Q           quiet mode (no prompt) and quit faster (quickLeak=true)
  -p [prj]     use project, list if no arg, load if no file
  -P [file]    apply rapatch file and quit
  -r [rarun2]  specify rarun2 profile to load (same as -e dbg.profile=X)
  -R [rr2rule] specify custom rarun2 directive
  -s [addr]    initial seek
  -S           start r2 in sandbox mode
  -t           load rabin2 info in thread
  -u           set bin.filter=false to get raw sym/sec/cls names
  -v, -V       show radare2 version (-V show lib versions)
  -w           open file in write mode
  -x           open without exec-flag (asm.emu will not work), See io.exec
  -X           same as -e bin.usextr=false (useful for dyldcache)
  -z, -zz      do not load strings or load them even in raw
 ```
 
 - - -
 
 ##### radiff2
 
 Binary diffing utility
 
 ```
 root@kali:~# radiff2 -h
 Usage: radiff2 [-1abcCdeGhijnropqsSxuUvVzZ] [-A[A]] [-B #] [-g sym] [-m graph_mode][-t %] [file] [file]
   -a [arch]  specify architecture plugin to use (x86, arm, ..)
   -A [-A]    run aaa or aaaa after loading each binary (see -C)
   -b [bits]  specify register size for arch (16 (thumb), 32, 64, ..)
   -B [baddr] define the base address to add the offsets when listing
   -1         output in Generic binary DIFF (0xd1ffd1ff magic header)
   -c         count of changes
   -C         graphdiff code (columns: off-A, match-ratio, off-B) (see -A)
   -d         use delta diffing
   -D         show disasm instead of hexpairs
   -e [k=v]   set eval config var value for all RCore instances
   -g [arg]   graph diff of [sym] or functions in [off1,off2]
   -G [cmd]   run an r2 command on every RCore instance created
   -i [ifscm] diff imports | fields | symbols | classes | methods
   -j         output in json format
   -n         print bare addresses only (diff.bare=1)
   -m [mode]  choose the graph output mode (aditsjJ)
   -O         code diffing with opcode bytes only
   -p         use physical addressing (io.va=false) (only for radiff2 -AC)
   -q         quiet mode (disable colors, reduce output)
   -r         output in radare commands
   -s         compute edit distance (no substitution, Eugene W. Myers' O(ND) diff algorithm)
   -ss        compute Levenshtein edit distance (substitution is allowed, O(N^2))
   -S [name]  sort code diff (name, namelen, addr, size, type, dist) (only for -C or -g)
   -t [0-100] set threshold for code diff (default is 70%)
   -T         analyze files in threads (EXPERIMENTAL, 30% faster and crashy)
   -x         show two column hexdump diffing
   -X         show two column hexII diffing
   -u         unified output (---+++)
   -U         unified output using system 'diff'
   -v         show version information
   -V         be verbose (current only for -s)
   -z         diff on extracted strings
   -Z         diff code comparing zignatures
 
 Graph Output formats: (-m [mode])
   <blank/a>  ascii art
   s          r2 commands
   d          graphviz dot
   g          graph Modelling Language (gml)
   j          json
   J          json with disarm
   k          sdb key-value
   t          tiny ascii art
   i          interactive ascii art
 ```
 
 - - -
 
 ##### rafind2
 
 Advanced command-line byte pattern search in files
 
 ```
 root@kali:~# rafind2 -h
 Usage: rafind2 [-mBXnzZhqv] [-a align] [-b sz] [-f/t from/to] [-[e|s|S] str] [-x hex] -|file|dir ..
  -a [align] only accept aligned hits
  -b [size]  set block size
  -B         use big endian instead of the little one (See -V)
  -c         disable colourful output (mainly for for -X)
  -e [regex] search for regex matches (can be used multiple times)
  -E         perform a search using an esil expression
  -f [from]  start searching from address 'from'
  -F [file]  read the contents of the file and use it as keyword
  -h         show this help
  -i         identify filetype (r2 -nqcpm file)
  -j         output in JSON
  -L         list all io plugins (same as r2 for now)
  -m         magic search, file-type carver
  -M [str]   set a binary mask to be applied on keywords
  -n         do not stop on read errors
  -r         print using radare commands
  -s [str]   search for a string (more than one string can be passed)
  -S [str]   search for a wide string (more than one string can be passed).
  -t [to]    stop search at address 'to'
  -q         quiet: fewer output do not show headings or filenames.
  -v         print version and exit
  -V [s:num] search for given value in given endian (-V 4:123)
  -x [hex]   search for hexpair string (909090) (can be used multiple times)
  -X         show hexdump of search results
  -z         search for zero-terminated strings
  -Z         show string found on each search hit
 ```
 
 - - -
 
 ##### ragg2
 
 Radare2 frontend for r_egg, compile programs into tiny binaries for x86-32/64 and arm.
 
 ```
 root@kali:~# ragg2 -h
 Usage: ragg2 [-FOLsrxhvz] [-a arch] [-b bits] [-k os] [-o file] [-I path]
              [-i sc] [-E enc] [-B hex] [-c k=v] [-C file] [-p pad] [-q off]
              [-S string] [-f fmt] [-nN dword] [-dDw off:hex] [-e expr] file|f.asm|-
  -a [arch]       select architecture (x86, mips, arm)
  -b [bits]       register size (32, 64, ..)
  -B [hexpairs]   append some hexpair bytes
  -c [k=v]        set configuration options
  -C [file]       append contents of file
  -d [off:dword]  patch dword (4 bytes) at given offset
  -D [off:qword]  patch qword (8 bytes) at given offset
  -e [egg-expr]   take egg program from string instead of file
  -E [encoder]    use specific encoder. see -L
  -f [format]     output format (raw, c, pe, elf, mach0, python, javascript)
  -F              output native format (osx=mach0, linux=elf, ..)
  -h              show this help
  -i [shellcode]  include shellcode plugin, uses options. see -L
  -I [path]       add include path
  -k [os]         operating system's kernel (linux,bsd,osx,w32)
  -L              list all plugins (shellcodes and encoders)
  -n [dword]      append 32bit number (4 bytes)
  -N [dword]      append 64bit number (8 bytes)
  -o [file]       output file
  -O              use default output file (filename without extension or a.out)
  -p [padding]    add padding after compilation (padding=n10s32)
                  ntas : begin nop, trap, 'a', sequence
                  NTAS : same as above, but at the end
  -P [size]       prepend debruijn pattern
  -q [fragment]   debruijn pattern offset
  -r              show raw bytes instead of hexpairs
  -s              show assembler
  -S [string]     append a string
  -v              show version
  -w [off:hex]    patch hexpairs at given offset
  -x              execute
  -X [hexpairs]   execute rop chain, using the stack provided
  -z              output in C string syntax
 R2_NOPLUGINS=1   do not load any plugin
 ```
 
 - - -
 
 ##### rahash2
 
 Block-based hashing, encoding and encryption utility
 
 ```
 root@kali:~# rahash2 -h
 Usage: rahash2 [-BehjkLqrvX] [-b S] [-a A] [-c H] [-E A] [-s S] [-f O] [-t O] [file] ...
  -a algo     comma separated list of algorithms (default is 'sha256')
  -b bsize    specify the size of the block (instead of full file)
  -B          show per-block hash
  -c hash     compare with this hash
  -e          swap endian (use little endian)
  -E algo     encrypt. Use -S to set key and -I to set IV
  -D algo     decrypt. Use -S to set key and -I to set IV
  -f from     start hashing at given address
  -i num      repeat hash N iterations (f.ex: 3DES)
  -I iv       use give initialization vector (IV) (hexa or s:string)
  -j          output in json
  -J          new simplified json output (same as -jj)
  -S seed     use given seed (hexa or s:string) use ^ to prefix (key for -E)
              (- will slurp the key from stdin, the @ prefix points to a file
  -k          show hash using the openssh's randomkey algorithm
  -q          run in quiet mode (-qq to show only the hash)
  -L          list crypto plugins (combines with -q, used by -a, -E and -D)
  -r          output radare commands
  -s string   hash this string instead of files
  -t to       stop hashing at given address
  -x hexstr   hash this hexpair string instead of files
  -X          output in hexpairs instead of binary/plain
  -v          show version information
 ```
 
 - - -
 
 ##### rarun2
 
 Radare2 utility to run programs in exotic environments
 
 ```
 root@kali:~# rarun2 -h
 Usage: rarun2 -v|-t|script.rr2 [directive ..]
 program=/bin/ls
 arg1=/bin
 # arg2=hello
 # arg3="hello\nworld"
 # arg4=:048490184058104849
 # arg5=:!ragg2 -p n50 -d 10:0x8048123
 # arg6=@arg.txt
 # arg7=@300@ABCD # 300 chars filled with ABCD pattern
 # system=r2 -
 # daemon=false
 # aslr=no
 setenv=FOO=BAR
 # unsetenv=FOO
 # clearenv=true
 # envfile=environ.txt
 timeout=3
 # timeoutsig=SIGTERM # or 15
 # connect=localhost:8080
 # listen=8080
 # pty=false
 # fork=true
 # bits=32
 # pid=0
 # pidfile=/tmp/foo.pid
 # #sleep=0
 # #maxfd=0
 # #execve=false
 # #maxproc=0
 # #maxstack=0
 # #core=false
 # #stdio=blah.txt
 # #stderr=foo.txt
 # stdout=foo.txt
 # stdin=input.txt # or !program to redirect input from another program
 # input=input.txt
 # chdir=/
 # chroot=/mnt/chroot
 # libpath=$PWD:/tmp/lib
 # r2preload=yes
 # preload=/lib/libfoo.so # you can load more than one lib by using this directive many times
 # setuid=2000
 # seteuid=2000
 # setgid=2001
 # setegid=2001
 # nice=5
 ```
 
 - - -
 
 ##### rasign2
 
 A tool for generating and managing binary file signatures
 
 ```
 root@kali:~# rasign2 -h
 Usage: rasign2 [options] [file]
  -a               make signatures from all .o files in the provided .a file
  -A[AAA]          same as r2 -A, the more 'A's the more analysis is performed
  -f               interpret the file as a FLIRT .sig file and dump signatures
  -h               help menu
  -j               show signatures in json
  -i script.r2     execute this script in the 
  -o sigs.sdb      add signatures to file, create if it does not exist
  -q               quiet mode
  -r               show output in radare commands
  -S               perform operation on sdb signature file ('-o -' to save to same file)
  -s signspace     save all signatures under this signspace
  -c               add collision signatures before writing file
  -v               show version information
  -m               merge/overwrite signatures with same name
 Examples:
   rasign2 -o libc.sdb libc.so.6
 ```
 
 - - -
 
 ##### rasm2
 
 Radare2 assembler and disassembler tool
 
 ```
 root@kali:~# rasm2 -h
 Usage: rasm2 [-ACdDehLBvw] [-a arch] [-b bits] [-o addr] [-s syntax]
              [-f file] [-F fil:ter] [-i skip] [-l len] 'code'|hex|0101b|-
  -a [arch]    set architecture to assemble/disassemble (see -L)
  -A           show Analysis information from given hexpairs
  -b [bits]    set cpu register size (8, 16, 32, 64) (RASM2_BITS)
  -B           binary input/output (-l is mandatory for binary input)
  -c [cpu]     select specific CPU (depends on arch)
  -C           output in C format
  -d, -D       disassemble from hexpair bytes (-D show hexpairs)
  -e           use big endian instead of little endian
  -E           display ESIL expression (same input as in -d)
  -f [file]    read data from file
  -F [in:out]  specify input and/or output filters (att2intel, x86.pseudo, ...)
  -h, -hh      show this help, -hh for long
  -i [len]     ignore/skip N bytes of the input buffer
  -j           output in json format
  -k [kernel]  select operating system (linux, windows, darwin, ..)
  -l [len]     input/Output length
  -L           list RAsm plugins: (a=asm, d=disasm, A=analyze, e=ESIL)
  -LL          list RAnal plugins (see anal.arch=?) combines with -j
  -LLL         list RArch plugins (see arch.arch=?) combines with -j
  -o,-@ [addr] set start address for code (default 0)
  -O [file]    output file name (rasm2 -Bf a.asm -O a)
  -N           same as r2 -N (or R2_NOPLUGINS) (not load any plugin)
  -p           run SPP over input for assembly
  -q           quiet mode
  -r           output in radare commands
  -s [syntax]  select syntax (intel, att)
  -v           show version information
  -x           use hex dwords instead of hex pairs when assembling.
  -w           what's this instruction for? describe opcode
  If '-l' value is greater than output length, output is padded with nops
  If the last argument is '-' reads from stdin
 Environment:
  R2_NOPLUGINS   do not load shared plugins (speedup loading)
  R2_LOG_LEVEL=X    change the log level
  R2_DEBUG          if defined, show error messages and crash signal
  R2_DEBUG_ASSERT=1 lldb -- r2 to get proper backtrace of the runtime assert
  RASM2_ARCH        same as rasm2 -a
  RASM2_BITS        same as rasm2 -b
 ```
 
 - - -
 
 ##### ravc2
 
 Radare version control
 
 ```
 root@kali:~# ravc2 -h
 Usage: ravc2 [-qvh] [action] [args ...]
 Flags:
  -q       quiet mode
  -v       show version
  -h       display this help message
  -j       json output
 Actions:
  init     [git | rvc]          initialize a repository with the given vc
  branch   [name]               if a name is provided, create a branch with that name otherwise list branches
  commit   [message] [files...] commit the files with the message
  checkout [branch]             set the current branch to the given branch
  status                        print a status message
  reset                         remove all uncommited changes
  log                           print all commits
  RAVC2_USER=[n]                override cfg.user value to author commit
 ```
 
 - - -
 
 ##### rax2
 
 Radare base converter
 
 ```
 root@kali:~# rax2 -h
 Usage: rax2 [-h|...] [- | expr ...] # convert between numeric bases
   =[base]                      ;  rax2 =10 0x46 -> output in base 10
   int     ->  hex              ;  rax2 10
   hex     ->  int              ;  rax2 0xa
   -int    ->  hex              ;  rax2 -77
   -hex    ->  int              ;  rax2 0xffffffb3
   int     ->  bin              ;  rax2 b30
   int     ->  ternary          ;  rax2 t42
   bin     ->  int              ;  rax2 1010d
   ternary ->  int              ;  rax2 1010dt
   float   ->  hex              ;  rax2 3.33f
   hex     ->  float            ;  rax2 Fx40551ed8
   oct     ->  hex              ;  rax2 35o
   hex     ->  oct              ;  rax2 Ox12 (O is a letter)
   bin     ->  hex              ;  rax2 1100011b
   hex     ->  bin              ;  rax2 Bx63
   ternary ->  hex              ;  rax2 212t
   hex     ->  ternary          ;  rax2 Tx23
   raw     ->  hex              ;  rax2 -S < /binfile
   hex     ->  raw              ;  rax2 -s 414141
   -a      show ascii table     ;  rax2 -a
   -b      bin -> str           ;  rax2 -b 01000101 01110110
   -B      str -> bin           ;  rax2 -B hello
   -c      output in C string   ;  rax2 -c 0x1234 # \x34\x12\x00\x00
   -d      force integer        ;  rax2 -d 3 -> 3 instead of 0x3
   -e      swap endianness      ;  rax2 -e 0x33
   -D      base64 decode        ;  rax2 -D "aGVsbG8="
   -E      base64 encode        ;  rax2 -E "hello"
   -f      floating point       ;  rax2 -f 6.3+2.1
   -F      stdin slurp code hex ;  rax2 -F < shellcode.[c/py/js]
   -h      help                 ;  rax2 -h
   -i      dump as C byte array ;  rax2 -i < bytes
   -I      IP address <-> LONG  ;  rax2 -I 3530468537
   -j      json format output   ;  rax2 -j 0x1234 # same as r2 -c '?j 0x1234'
   -k      keep base            ;  rax2 -k 33+3 -> 36
   -K      randomart            ;  rax2 -K 0x34 1020304050
   -L      bin -> hex(bignum)   ;  rax2 -L 111111111 # 0x1ff
   -n      newline              ;  append newline to output (for -E/-D/-r/..)
   -o      octalstr -> raw      ;  rax2 -o \162 \62 # r2
   -r      r2 style output      ;  rax2 -r 0x1234 # same as r2 -c '? 0x1234'
   -s      hexstr -> raw        ;  rax2 -s 43 4a 50
   -S      raw -> hexstr        ;  rax2 -S < /bin/ls > ls.hex
   -rS     raw -> hex.r2        ;  rax2 -rS < /bin/ls > ls.r2
   -t      tstamp -> str        ;  rax2 -t 1234567890
   -u      units                ;  rax2 -u 389289238 # 317.0M
   -v      version              ;  rax2 -v
   -w      signed word          ;  rax2 -w 16 0xffff
   -x      output in hexpairs   ;  rax2 -x 0x1234 # 34120000
   -X      hash string          ;  rax2 -X linux osx
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
