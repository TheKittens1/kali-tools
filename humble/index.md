---
Title: humble
Homepage: https://github.com/rfc-st/humble
Repository: https://gitlab.com/kalilinux/packages/humble
Architectures: all
Version: 1.35-0kali2
Metapackages: kali-linux-everything 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### humble
 
  This package contains an  humble, and fast, security-oriented HTTP headers
  analyzer.
 
 **Installed size:** `240 KB`  
 **How to install:** `sudo apt install humble`  
 
 {{< spoiler "Dependencies:" >}}
 * publicsuffix
 * python3
 * python3-colorama
 * python3-fpdf
 * python3-requests
 * python3-tldextract
 {{< /spoiler >}}
 
 ##### humble
 
 
 ```
 root@kali:~# humble -h
 usage: humble.py [-h] [-a] [-b] [-df] [-e [PATH]] [-f [TERM]] [-g] [-l {es}]
                  [-o {csv,html,json,pdf,txt}] [-op OUTPUT_PATH] [-r]
                  [-s [SKIPPED_HEADERS ...]] [-u URL] [-ua USER_AGENT] [-v]
 
 'humble' (HTTP Headers Analyzer) | https://github.com/rfc-st/humble | v.2024-03-27
 
 options:
   -h, --help                  show this help message and exit
   -a                          Shows statistics of the performed analysis (will
                               be global if '-u' is omitted)
   -b                          Shows overall findings (if omitted, details will
                               be shown)
   -df                         Do not follow redirects (if omitted, the last
                               redirection will be the one analyzed)
   -e [PATH]                   Shows TLS/SSL checks (requires the PATH of
                               https://testssl.sh/ and Linux/Unix OS)
   -f [TERM]                   Shows fingerprint statistics (will be the Top 20
                               if "TERM", e.g. "Google", is omitted)
   -g                          Shows guidelines for enabling security HTTP
                               response headers on popular servers/services
   -l {es}                     The language for displaying analysis, errors and
                               messages (if omitted it will be in English)
   -o {csv,html,json,pdf,txt}  Exports analysis to
                               'scheme_host_port_yyyymmdd.ext' file (csv/json
                               files will contain a brief analysis)
   -op OUTPUT_PATH             Exports analysis to OUTPUT_PATH (if omitted, the
                               PATH of 'humble.py' will be used)
   -r                          Shows HTTP response headers and a detailed
                               analysis ('-b' parameter will take priority)
   -s [SKIPPED_HEADERS ...]    Skip analysis of specified HTTP response
                               headers, separated by spaces
   -u URL                      Scheme, host and port to analyze. E.g.
                               https://google.com
   -ua USER_AGENT              User-Agent ID from 'additional/user_agents.txt'
                               to use. '0' will show all and '1' is the
                               default.
   -v, --version               Checks for updates at https://github.com/rfc-
                               st/humble
 
 examples:
   -u URL -b                   Analyzes the URL and reports overall findings
   -u URL -r                   Analyzes the URL and reports detailed findings along with HTTP response headers
   -u URL -l es                Analyzes the URL and reports detailed findings in Spanish
   -u URL -b -o csv            Analyzes the URL and exports overall findings to CSV
   -u URL -o pdf               Analyzes the URL and exports detailed findings to PDF
   -u URL -a                   Shows statistics of the analysis performed against the URL
   -u URL -s ETag NEL          Analyzes the URL and skips checks associated with 'ETag' and 'NEL' HTTP response headers
   -a -l es                    Shows statistics of the analysis performed against all URLs in Spanish
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
