---
Title: juice-shop
Homepage: https://github.com/juice-shop/juice-shop
Repository: https://gitlab.com/kalilinux/packages/juice-shop
Architectures: amd64
Version: 16.0.1-0kali1
Metapackages: kali-linux-labs 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### juice-shop
 
  This package contains a modern and sophisticated insecure web application! It
  can be used in security trainings, awareness demos, CTFs and as a guinea pig
  for security tools! Juice Shop encompasses vulnerabilities from the entire
  OWASP Top Ten along with many other security flaws found in real-world
  applications!
   
  WARNING: Do not upload it to your hosting provider's public html folder or any
  Internet facing servers, as they will be compromised.
 
 **Installed size:** `1.22 GB`  
 **How to install:** `sudo apt install juice-shop`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser
 * libc6 
 * libgcc-s1 
 * libnode109 
 * libstdc++6 
 * lsof
 * npm
 * xdg-utils
 {{< /spoiler >}}
 
 ##### juice-shop
 
 
 ```
 root@kali:~# juice-shop -h
 [*] Please wait for the Juice-shop service to start.
 [*]
 [*] You might need to refresh your browser once it opens.
 [*]
 [*]  Web UI: http://127.0.0.1:42000
 
 ```
 
 - - -
 
 ##### juice-shop-stop
 
 
 ```
 root@kali:~# juice-shop-stop -h
 * juice-shop.service - juice-shop web application
      Loaded: loaded (/usr/lib/systemd/system/juice-shop.service; disabled; preset: disabled)
      Active: inactive (dead)
 
 May 29 10:20:22 kali npm[400190]: info: Required file polyfills.js is present (OK)
 May 29 10:20:22 kali npm[400190]: info: Required file runtime.js is present (OK)
 May 29 10:20:22 kali npm[400190]: info: Required file vendor.js is present (OK)
 May 29 10:20:22 kali npm[400190]: info: Port 42000 is available (OK)
 May 29 10:20:22 kali npm[400190]: info: Chatbot training data botDefaultTrainingData.json validated (OK)
 May 29 10:20:22 kali npm[400190]: info: Domain https://www.alchemy.com/ is reachable (OK)
 May 29 10:20:34 kali systemd[1]: Stopping juice-shop.service - juice-shop web application...
 May 29 10:20:34 kali systemd[1]: juice-shop.service: Deactivated successfully.
 May 29 10:20:34 kali systemd[1]: Stopped juice-shop.service - juice-shop web application.
 May 29 10:20:34 kali systemd[1]: juice-shop.service: Consumed 3.671s CPU time.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
