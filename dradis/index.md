---
Title: dradis
Homepage: https://dradis.com/ce/
Repository: https://gitlab.com/kalilinux/packages/dradis
Architectures: amd64
Version: 4.12.0-0kali1
Metapackages: kali-linux-everything kali-linux-large kali-tools-reporting 
Icon: images/dradis-logo.svg
PackagesInfo: |
 ### dradis
 
  Dradis is a tool to help you simplify reporting and collaboration.
   
    - Spend more time testing and less time reporting
    - Ensure consistent quality across your assessments
    - Combine the output of your favorite scanners
   
   Dradis is an open-source project released in 2007 that has been refined for
   over a decade by security professionals around the world.
 
 **Installed size:** `448.50 MB`  
 **How to install:** `sudo apt install dradis`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser
 * bundler
 * git
 * libc6 
 * libgcc-s1 
 * libpq5 
 * libruby3.1t64 
 * libsqlite3-0 
 * libssl3t64 
 * libstdc++6 
 * lsof
 * pwgen
 * ruby 
 {{< /spoiler >}}
 
 ##### dradis
 
 
 ```
 root@kali:~# dradis -h
 [i] Something is already using port: 3000/tcp
 COMMAND    PID   USER   FD   TYPE DEVICE SIZE/OFF NODE NAME
 ruby3.1 112592 dradis   20u  IPv4 257795      0t0  TCP localhost:3000 (LISTEN)
 ruby3.1 112592 dradis   21u  IPv6 257796      0t0  TCP localhost:3000 (LISTEN)
 
 UID          PID    PPID  C STIME TTY      STAT   TIME CMD
 dradis    112592       1 36 08:37 ?        Ssl    0:01 puma 6.4.2 (tcp://localhost:3000
 
 [*] Please wait for the Dradis service to start.
 [*]
 [*] You might need to refresh your browser once it opens.
 [*]
 [*]  Web UI: http://127.0.0.1:3000
 
 ```
 
 - - -
 
 ##### dradis-stop
 
 
 ```
 root@kali:~# dradis-stop -h
 * dradis.service - Dradis web application
      Loaded: loaded (/usr/lib/systemd/system/dradis.service; disabled; preset: disabled)
      Active: inactive (dead)
 
 May 29 08:37:21 kali bundle[112592]: *          PID: 112592
 May 29 08:37:21 kali bundle[112592]: * Listening on http://127.0.0.1:3000
 May 29 08:37:21 kali bundle[112592]: * Listening on http://[::1]:3000
 May 29 08:37:21 kali bundle[112592]: Use Ctrl-C to stop
 May 29 08:37:34 kali bundle[112592]: - Gracefully stopping, waiting for requests to finish
 May 29 08:37:34 kali systemd[1]: Stopping dradis.service - Dradis web application...
 May 29 08:37:34 kali bundle[112592]: Exiting
 May 29 08:37:34 kali systemd[1]: dradis.service: Deactivated successfully.
 May 29 08:37:34 kali systemd[1]: Stopped dradis.service - Dradis web application.
 May 29 08:37:34 kali systemd[1]: dradis.service: Consumed 1.982s CPU time.
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}


```console
service dradis start
```

## Screenshots

![Dradis login screen](images/dradis-01.png)
![Dradis dashboard](images/dradis-02.png)
![Dradis issue list](images/dradis-03.png)
![Dradis methodologies](images/dradis-04.png)
