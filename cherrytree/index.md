---
Title: cherrytree
Homepage: https://www.giuspen.net/cherrytree/
Repository: https://salsa.debian.org/debian/cherrytree
Architectures: any
Version: 1.1.2+dfsg-1
Metapackages: kali-linux-default kali-linux-everything kali-linux-large 
Icon: images/cherrytree-logo.svg
PackagesInfo: |
 ### cherrytree
 
  CherryTree is a hierarchical note taking application, featuring rich text,
  syntax highlighting, images handling, hyperlinks, import/export with support
  for multiple formats, support for multiple languages, and more.
 
 **Installed size:** `11.24 MB`  
 **How to install:** `sudo apt install cherrytree`  
 
 {{< spoiler "Dependencies:" >}}
 * desktop-file-utils
 * libatkmm-1.6-1v5 
 * libc6 
 * libcairo2 
 * libcairomm-1.0-1v5 
 * libcurl4t64 
 * libfmt9 
 * libfribidi0 
 * libgcc-s1 
 * libglib2.0-0t64 
 * libglibmm-2.4-1t64 
 * libgspell-1-2 
 * libgtk-3-0t64 
 * libgtkmm-3.0-1t64 
 * libgtksourceviewmm-3.0-0v5 
 * libpango-1.0-0 
 * libpangomm-1.4-1v5 
 * libsigc++-2.0-0v5 
 * libsqlite3-0 
 * libstdc++6 
 * libuchardet0 
 * libvte-2.91-0 
 * libxml++2.6-2v5 
 * libxml2 
 {{< /spoiler >}}
 
 ##### cherrytree
 
 A hierarchical note taking application
 
 ```
 root@kali:~# man cherrytree
 CHERRYTREE(1)               General Commands Manual               CHERRYTREE(1)
 
 NAME
        cherrytree - a hierarchical note taking application
 
 SYNOPSIS
        cherrytree  [-V]  [-N]  [filepath  [-n nodename] [-a anchorname] [-x ex-
        port_to_html_dir] [-t  export_to_txt_dir]  [-p  export_to_pdf_path]  [-P
        password] [-w] [-s]]
 
 DESCRIPTION
        cherrytree  is  a  hierarchical  note taking application, featuring rich
        text, syntax highlighting, images  handling,  hyperlinks,  import/export
        with  support  for multiple formats, support for multiple languages, and
        more.
 
 AUTHOR
        cherrytree  was  written  by  Giuseppe  Penone  <giuspen@gmail.com>  and
        Evgenii Gurianov <https://github.com/txe>.
 
        This  manual page was written by Vincent Cheng <Vincentc1208@gmail.com>,
        for the Debian project (and may be used by others).
 
 cherrytree 0.99.49               September 2022                   CHERRYTREE(1)
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}

## Screenshots

![cherrytree](images/cherrytree.png)
