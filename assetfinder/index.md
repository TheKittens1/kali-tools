---
Title: assetfinder
Homepage: https://github.com/tomnomnom/assetfinder
Repository: https://salsa.debian.org/pkg-security-team/assetfinder
Architectures: any
Version: 0.1.1-1
Metapackages: kali-linux-everything kali-tools-identify 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### assetfinder
 
  assetfinder is a command-line tool designed to find domains and
  subdomains associated with a specific domain.
   
  The main objective of the tool is to help security researchers and IT
  professionals discover and understand how the domains and sub-domains
  of a given organization are distributed, trying to find possible
  security flaws and vulnerabilities.
   
  assetfinder uses multiple data sources to perform its research, including:
   - crt.sh
   - certspotter
   - hackertarget
   - threatcrowd
   - Wayback Machine
   - dns.bufferover.run
   - Facebook Graph API
   - Virustotal
   - findsubdomains
  This expands coverage and increases the accuracy of results.
 
 **Installed size:** `4.92 MB`  
 **How to install:** `sudo apt install assetfinder`  
 
 {{< spoiler "Dependencies:" >}}
 * libc6 
 {{< /spoiler >}}
 
 ##### assetfinder
 
 
 ```
 root@kali:~# assetfinder -h
 Usage of assetfinder:
   -subs-only
     	Only include subdomains of search domain
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
