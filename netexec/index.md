---
Title: netexec
Homepage: https://github.com/Pennyw0rth/NetExec
Repository: https://gitlab.com/kalilinux/packages/netexec
Architectures: all
Version: 1.2.0+git20240529.7ece667-0kali2
Metapackages: 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### netexec
 
  NetExec (AKA nxc) is a network service exploitation tool that helps automate
  assessing the security of large networks.
   
  NetExec is the continuation of CrackMapExec, which was maintained
  by mpgn over the years, but discontinued upon mpgn's retirement.
 
 **Installed size:** `3.15 MB`  
 **How to install:** `sudo apt install netexec`  
 
 {{< spoiler "Dependencies:" >}}
 * bloodhound.py
 * libkrb5-dev
 * python3
 * python3-aardwolf
 * python3-aioconsole
 * python3-aiosqlite 
 * python3-argcomplete 
 * python3-asyauth
 * python3-bs4 
 * python3-dateutil 
 * python3-dploot
 * python3-dsinternals
 * python3-impacket 
 * python3-libnmap 
 * python3-lsassy
 * python3-masky
 * python3-minikerberos
 * python3-msgpack 
 * python3-msldap
 * python3-neo4j
 * python3-paramiko 
 * python3-poetry-dynamic-versioning
 * python3-pyasn1-modules 
 * python3-pylnk3
 * python3-pypsrp
 * python3-pypykatz
 * python3-pywerview
 * python3-requests 
 * python3-rich 
 * python3-sqlalchemy 
 * python3-termcolor 
 * python3-terminaltables 
 * python3-xmltodict 
 {{< /spoiler >}}
 
 ##### netexec
 
 
 ```
 root@kali:~# netexec -h
 usage: netexec [-h] [-t THREADS] [--timeout TIMEOUT] [--jitter INTERVAL]
                [--verbose] [--debug] [--no-progress] [--log LOG] [-6]
                [--dns-server DNS_SERVER] [--dns-tcp]
                [--dns-timeout DNS_TIMEOUT] [--version]
                {vnc,smb,winrm,mssql,ftp,ssh,rdp,wmi,ldap} ...
 
      .   .
     .|   |.     _   _          _     _____
     ||   ||    | \ | |   ___  | |_  | ____| __  __   ___    ___
     \\( )//    |  \| |  / _ \ | __| |  _|   \ \/ /  / _ \  / __|
     .=[ ]=.    | |\  | |  __/ | |_  | |___   >  <  |  __/ | (__
    / /ॱ-ॱ\ \   |_| \_|  \___|  \__| |_____| /_/\_\  \___|  \___|
    ॱ \   / ॱ
      ॱ   ॱ
 
     The network execution tool
     Maintained as an open source project by @NeffIsBack, @MJHallenbeck, @_zblurx
     
     For documentation and usage examples, visit: https://www.netexec.wiki/
 
     Version : 1.2.0
     Codename: ItsAlwaysDNS
     Commit  : kali
     
 
 options:
   -h, --help            show this help message and exit
   --version             Display nxc version
 
 Generic:
   Generic options for nxc across protocols
 
   -t THREADS, --threads THREADS
                         set how many concurrent threads to use
   --timeout TIMEOUT     max timeout in seconds of each thread
   --jitter INTERVAL     sets a random delay between each authentication
 
 Output:
   Options to set verbosity levels and control output
 
   --verbose             enable verbose output
   --debug               enable debug level information
   --no-progress         do not displaying progress bar during scan
   --log LOG             export result into a custom file
 
 DNS:
   -6                    Enable force IPv6
   --dns-server DNS_SERVER
                         Specify DNS server (default: Use hosts file & System DNS)
   --dns-tcp             Use TCP instead of UDP for DNS queries
   --dns-timeout DNS_TIMEOUT
                         DNS query timeout in seconds
 
 Available Protocols:
   {vnc,smb,winrm,mssql,ftp,ssh,rdp,wmi,ldap}
     vnc                 own stuff using VNC
     smb                 own stuff using SMB
     winrm               own stuff using WINRM
     mssql               own stuff using MSSQL
     ftp                 own stuff using FTP
     ssh                 own stuff using SSH
     rdp                 own stuff using RDP
     wmi                 own stuff using WMI
     ldap                own stuff using LDAP
 ```
 
 - - -
 
 ##### nxc
 
 
 ```
 root@kali:~# nxc -h
 usage: nxc [-h] [-t THREADS] [--timeout TIMEOUT] [--jitter INTERVAL]
            [--verbose] [--debug] [--no-progress] [--log LOG] [-6]
            [--dns-server DNS_SERVER] [--dns-tcp] [--dns-timeout DNS_TIMEOUT]
            [--version]
            {vnc,smb,winrm,mssql,ftp,ssh,rdp,wmi,ldap} ...
 
      .   .
     .|   |.     _   _          _     _____
     ||   ||    | \ | |   ___  | |_  | ____| __  __   ___    ___
     \\( )//    |  \| |  / _ \ | __| |  _|   \ \/ /  / _ \  / __|
     .=[ ]=.    | |\  | |  __/ | |_  | |___   >  <  |  __/ | (__
    / /ॱ-ॱ\ \   |_| \_|  \___|  \__| |_____| /_/\_\  \___|  \___|
    ॱ \   / ॱ
      ॱ   ॱ
 
     The network execution tool
     Maintained as an open source project by @NeffIsBack, @MJHallenbeck, @_zblurx
     
     For documentation and usage examples, visit: https://www.netexec.wiki/
 
     Version : 1.2.0
     Codename: ItsAlwaysDNS
     Commit  : kali
     
 
 options:
   -h, --help            show this help message and exit
   --version             Display nxc version
 
 Generic:
   Generic options for nxc across protocols
 
   -t THREADS, --threads THREADS
                         set how many concurrent threads to use
   --timeout TIMEOUT     max timeout in seconds of each thread
   --jitter INTERVAL     sets a random delay between each authentication
 
 Output:
   Options to set verbosity levels and control output
 
   --verbose             enable verbose output
   --debug               enable debug level information
   --no-progress         do not displaying progress bar during scan
   --log LOG             export result into a custom file
 
 DNS:
   -6                    Enable force IPv6
   --dns-server DNS_SERVER
                         Specify DNS server (default: Use hosts file & System DNS)
   --dns-tcp             Use TCP instead of UDP for DNS queries
   --dns-timeout DNS_TIMEOUT
                         DNS query timeout in seconds
 
 Available Protocols:
   {vnc,smb,winrm,mssql,ftp,ssh,rdp,wmi,ldap}
     vnc                 own stuff using VNC
     smb                 own stuff using SMB
     winrm               own stuff using WINRM
     mssql               own stuff using MSSQL
     ftp                 own stuff using FTP
     ssh                 own stuff using SSH
     rdp                 own stuff using RDP
     wmi                 own stuff using WMI
     ldap                own stuff using LDAP
 ```
 
 - - -
 
 ##### nxcdb
 
 
 ```
 root@kali:~# nxcdb -h
 usage: nxcdb [-h] [-gw] [-cw CREATE_WORKSPACE] [-sw SET_WORKSPACE]
 
 NXCDB is a database navigator for NXC
 
 options:
   -h, --help            show this help message and exit
   -gw, --get-workspace  get the current workspace
   -cw CREATE_WORKSPACE, --create-workspace CREATE_WORKSPACE
                         create a new workspace
   -sw SET_WORKSPACE, --set-workspace SET_WORKSPACE
                         set the current workspace
 ```
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
